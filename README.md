# HARP-FE Plugin for FEBio

HARP-FE stands for *harmonic phase with finite elements*, and it is an algorithm for approximating soft tissue deformation using tagged MRI and a mechancial finite-element model. The source code is compatible with FEBio 2.3. The files within this project also include makefiles for linux as well as an example demonstrating the usage of the algorithm. 