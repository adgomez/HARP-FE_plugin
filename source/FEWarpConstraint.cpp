#include "stdafx.h"
#include "FEWarpConstraint.h"
#include <FEBioMech/FEElasticMaterial.h>
#include <FECore/FEModel.h>
#include <FECore/FECoreKernel.h>
#include <FECore/Logfile.h>
#include <iostream>
using namespace std;

extern FECoreKernel* pFEBio;

//=============================================================================

FEWarpConstraint::FEWarpConstraint(FEModel* pfem) : FENLConstraint(pfem)
{
	m_blaugon = false;
	m_altol = 0.1;
	m_k = 0;
}

//-----------------------------------------------------------------------------
FEWarpConstraint::~FEWarpConstraint(void)
{

}

//-----------------------------------------------------------------------------
bool FEWarpConstraint::Init()
{
	FEModel& fem = *GetFEModel();
	FEMesh& mesh = fem.GetMesh();

	// if no domains are selected, add all domains
        // TODO: set up limit ("include" of "ignore" certain materials)
	if (m_dom.empty()) 
	{
		for (int i=0; i<mesh.Domains(); ++i) m_dom.push_back(i);
	}

	// figure out how many integration points we need
	int nint = 0;
	int ND = m_dom.size();
	for (int i=0; i<ND; ++i)
	{
		FESolidDomain& dom = dynamic_cast<FESolidDomain&>(mesh.Domain(m_dom[i]));
		int NE = dom.Elements();
		for (int j=0; j<NE; ++j)
		{
			FESolidElement& el = dom.Element(j);
			nint += el.GaussPoints();
		}
	}

	// allocate storage for Lagrange multipliers
	m_Lm.assign(nint, vec3d(0,0,0));
	
	// delayed update function
	// set update flag
	m_update_complete = false;

	// setup container
	int N = mesh.Nodes();
	N_data.resize(N);
	for (int i=0; i<N; ++i) 
	{
		if (0<=m_update_t){
			N_data[i] = mesh.Node(i).m_r0;
		}
		else{
			N_data[i] = mesh.Node(i).m_v0;
		}
	}
	return true;
}

//-----------------------------------------------------------------------------
void FEWarpConstraint::Residual(FEGlobalVector& R)
{

	FEModel& fem = *GetFEModel();
	FEMesh& mesh = fem.GetMesh();

	vector<double> fe;
	vector<int> lm;

	// reset multiplier counter
	m_nint = 0;

	// delayed update function
	double m_t = fem.GetTime().t;
	double m_dt = fem.GetTime().dt;

	if ((0<=m_update_t)&&(m_t - m_dt <= m_update_t)&&(m_update_complete == false))
	{
		cout << " ****** HARP-FE: updating reference at " << m_update_t << endl;
		int N = mesh.Nodes();
		for (int i=0; i<N; ++i) 
		{
			N_data[i] = mesh.Node(i).m_rt;
		}
		m_update_complete = true;
	}
	else
	{

        }

	// loop over all domains
	int NDOM = m_dom.size();
	for (int n=0; n<NDOM; ++n)
	{
		FESolidDomain& dom = dynamic_cast<FESolidDomain&>(mesh.Domain(m_dom[n]));

		// don't forget to multiply with the density
		FESolidMaterial* pme = dynamic_cast<FESolidMaterial*>(dom.GetMaterial());
		double dens = pme->Density();

		int NEL = dom.Elements();
		for (int i=0; i<NEL; ++i)
		{
			FESolidElement& el = dom.Element(i);

			int ndof = 3*el.Nodes();
			fe.assign(ndof, 0);

			// apply body forces
			ElementWarpForce(dom, el, fe, dens);

			// get the element's LM vector
			dom.UnpackLM(el, lm);

			// assemble element 'fe'-vector into global R vector
			R.Assemble(el.m_node, lm, fe);
		}
	}
}

//-----------------------------------------------------------------------------
void FEWarpConstraint::ElementWarpForce(FESolidDomain& dom, FESolidElement& el, vector<double>& fe, double dens)
{
	FEModel& fem = *GetFEModel();
	FEMesh& mesh = fem.GetMesh();

	// jacobian
	double detJ;
	double *H;
	double* gw = el.GaussWeights();
	vec3d f;
	vec3d f_warp;

	// number of nodes
	int neln = el.Nodes();

	// nodal coordinates
	vec3d r0[FEElement::MAX_NODES], rt[FEElement::MAX_NODES];
	for (int i=0; i<neln; ++i)
	{
		// delayed update function
		r0[i] = N_data[el.m_node[i]];

		// r0[i] = mesh.Node(el.m_node[i]).m_r0;
		rt[i] = mesh.Node(el.m_node[i]).m_rt;
	}			

	// loop over integration points
	int nint = el.GaussPoints();
	for (int n=0; n<nint; ++n)
	{
		FEMaterialPoint& mp = *el.GetMaterialPoint(n);
		FEElasticMaterialPoint& pt = *mp.ExtractData<FEElasticMaterialPoint>();
			
		pt.m_r0 = el.Evaluate(r0, n);
		pt.m_rt = el.Evaluate(rt, n);

		detJ = dom.detJ0(el, n)*gw[n];

		// get warping force
		f_warp = wrpForce(pt);

		// scale according to material % this should be removed
		
		if (el.GetMatID() > 0){
			f_warp.x = f_warp.x*1e-5;
			f_warp.y = f_warp.y*1e-5;
			f_warp.z = f_warp.z*1e-5;
		}
		
		// get the force
		f = m_Lm[m_nint++] + f_warp;

		// condense
		H = el.H(n);
		for (int i=0; i<neln; ++i)
		{
			fe[3*i  ] -= H[i]*dens*f.x*detJ;
			fe[3*i+1] -= H[i]*dens*f.y*detJ;
			fe[3*i+2] -= H[i]*dens*f.z*detJ;
		}						
	}
}

//-----------------------------------------------------------------------------
void FEWarpConstraint::StiffnessMatrix(FESolver* psolver)
{
	FEModel& fem = *GetFEModel();
	FEMesh& mesh = fem.GetMesh();

	// element stiffness matrix
	matrix ke;
	vector<int> lm;

	// loop over all domains
	int NDOM = m_dom.size();
	for (int n=0; n<NDOM; ++n)
	{
		FESolidDomain& dom = dynamic_cast<FESolidDomain&>(mesh.Domain(m_dom[n]));

		// don't forget to multiply with the density
		FESolidMaterial* pme = dynamic_cast<FESolidMaterial*>(dom.GetMaterial());
		double dens = pme->Density();

		// repeat over all solid elements
		int NE = dom.Elements();
		for (int iel=0; iel<NE; ++iel)
		{
			// if an element has nodes outside the image, they should be skipped
			FESolidElement& el = dom.Element(iel);

			// create the element's stiffness matrix
			int ndof = 3*el.Nodes();
			ke.resize(ndof, ndof);
			ke.zero();

			// calculate inertial stiffness
			ElementWarpStiffness(dom, el, ke, dens);

			// get the element's LM vector
			dom.UnpackLM(el, lm);

			// assemble element matrix in global stiffness matrix
			psolver->AssembleStiffness(el.m_node, lm, ke);
		}
	}
}

//-----------------------------------------------------------------------------
void FEWarpConstraint::ElementWarpStiffness(FESolidDomain& dom, FESolidElement& el, matrix& ke, double dens)
{
	int neln = el.Nodes();
	int ndof = ke.columns()/neln;

	// jacobian
	double detJ;
	double *H;
	double* gw = el.GaussWeights();

        // stiffness
	mat3d K;
	mat3d K_warp;

	// added since it appears to belong here
	FEModel& fem = *GetFEModel();
	FEMesh& mesh = fem.GetMesh();
	// nodal coordinates
	vec3d r0[FEElement::MAX_NODES], rt[FEElement::MAX_NODES];
	for (int i=0; i<neln; ++i)
	{
		// delayed update function
		r0[i] = N_data[el.m_node[i]];

		// r0[i] = mesh.Node(el.m_node[i]).m_r0;
		rt[i] = mesh.Node(el.m_node[i]).m_rt;
	}	

	// loop over integration points
	int nint = el.GaussPoints();
	for (int n=0; n<nint; ++n)
	{
		FEMaterialPoint& mp = *el.GetMaterialPoint(n);
		detJ = dom.detJ0(el, n)*gw[n];

		// added since it appears to belong here
		FEElasticMaterialPoint& pt = *mp.ExtractData<FEElasticMaterialPoint>();
		pt.m_r0 = el.Evaluate(r0, n);
		pt.m_rt = el.Evaluate(rt, n);

		// get the stiffness
		K_warp = wrpStiffness(pt);

		// scale according to material
		if (el.GetMatID() > 0){
			K_warp= K_warp*1e-5;
		}

		// add matrix
		K = K_warp;

		// condense
		H = el.H(n);
		for (int i=0; i<neln; ++i)
			for (int j=0; j<neln; ++j)
			{
				ke[ndof*i  ][ndof*j  ] -= H[i]*H[j]*dens*K(0,0)*detJ;
				ke[ndof*i  ][ndof*j+1] -= H[i]*H[j]*dens*K(0,1)*detJ;
				ke[ndof*i  ][ndof*j+2] -= H[i]*H[j]*dens*K(0,2)*detJ;

				ke[ndof*i+1][ndof*j  ] -= H[i]*H[j]*dens*K(1,0)*detJ;
				ke[ndof*i+1][ndof*j+1] -= H[i]*H[j]*dens*K(1,1)*detJ;
				ke[ndof*i+1][ndof*j+2] -= H[i]*H[j]*dens*K(1,2)*detJ;

				ke[ndof*i+2][ndof*j  ] -= H[i]*H[j]*dens*K(2,0)*detJ;
				ke[ndof*i+2][ndof*j+1] -= H[i]*H[j]*dens*K(2,1)*detJ;
				ke[ndof*i+2][ndof*j+2] -= H[i]*H[j]*dens*K(2,2)*detJ;
			}
	}	
}

//-----------------------------------------------------------------------------
bool FEWarpConstraint::Augment(int naug)
{
	if (m_blaugon == false) return true;

	FEModel& fem = *GetFEModel();
	FEMesh& mesh = fem.GetMesh();

	vector<vec3d> L0(m_Lm);
	vector<vec3d> L1(m_Lm);

	vec3d f_warp;

	m_nint = 0;
	int NDOM = m_dom.size();
	for (int i=0; i<NDOM; ++i)
	{
		FESolidDomain& dom = dynamic_cast<FESolidDomain&>(mesh.Domain(m_dom[i]));
		int NE = dom.Elements();
		for (int j=0; j<NE; ++j)
		{
			FESolidElement& el = dom.Element(j);
			int nint = el.GaussPoints();
			int neln = el.Nodes();

			// nodal coordinates
			vec3d r0[FEElement::MAX_NODES], rt[FEElement::MAX_NODES];
			for (int i=0; i<neln; ++i)
			{
				// delayed update function
				r0[i] = N_data[el.m_node[i]];

				rt[i] = mesh.Node(el.m_node[i]).m_rt;
			}

			for (int n=0; n<nint; ++n, ++m_nint)
			{
				FEMaterialPoint& mp = *el.GetMaterialPoint(n);
				FEElasticMaterialPoint& pt = *mp.ExtractData<FEElasticMaterialPoint>();
				pt.m_r0 = el.Evaluate(r0, n);
				pt.m_rt = el.Evaluate(rt, n);

				// get warping force
				f_warp = wrpForce(pt);

				L1[m_nint] = L0[m_nint] + f_warp;
			}
		}
	}

	// calculate the norm
	double normL0 = 0, normL1 = 0;
	for (int i=0; i<m_nint; ++i) 
	{
		normL0 += L0[i]*L0[i];
		normL1 += L1[i]*L1[i];
	}

	double Lerr = fabs((normL1 - normL0)/normL1);

	Logfile& felog = pFEBio->GetLogfile();
	felog.printf("warping norm: %lg %lg\n", Lerr, m_altol);

	if (Lerr >= m_altol)
	{
		// update multipliers
		m_Lm = L1;
	}

	// check convergence
	return (Lerr < m_altol);
}

//-----------------------------------------------------------------------------
void FEWarpConstraint::Update()
{

}
// ---------------------------------------------
void FEWarpConstraint::Serialize(DumpFile& ar)
{

}
