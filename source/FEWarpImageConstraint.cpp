#include "stdafx.h"
#include "FEWarpImageConstraint.h"
#include <FEBioMech/FEElasticMaterial.h>

//-----------------------------------------------------------------------------
BEGIN_PARAMETER_LIST(FEWarpImageConstraint, FEWarpConstraint);
	// phase images (required) 
	ADD_PARAMETER(m_tp1   , FE_PARAM_IMAGE_3D, "TP1"      );
	ADD_PARAMETER(m_tp2   , FE_PARAM_IMAGE_3D, "TP2"      );
	ADD_PARAMETER(m_tp3   , FE_PARAM_IMAGE_3D, "TP3"      );
	ADD_PARAMETER(m_sp1   , FE_PARAM_IMAGE_3D, "SP1"      );
	ADD_PARAMETER(m_sp2   , FE_PARAM_IMAGE_3D, "SP2"      );
	ADD_PARAMETER(m_sp3   , FE_PARAM_IMAGE_3D, "SP3"      );
        // magnitude tagged images (optional)
	ADD_PARAMETER(m_tm1   , FE_PARAM_IMAGE_3D, "TM1"     );
	ADD_PARAMETER(m_tm2   , FE_PARAM_IMAGE_3D, "TM2"     );
	ADD_PARAMETER(m_tm3   , FE_PARAM_IMAGE_3D, "TM3"     );
	ADD_PARAMETER(m_sm1   , FE_PARAM_IMAGE_3D, "SM1"     );
	ADD_PARAMETER(m_sm2   , FE_PARAM_IMAGE_3D, "SM2"     );
	ADD_PARAMETER(m_sm3   , FE_PARAM_IMAGE_3D, "SM3"     );
        // normal warping parameters
	ADD_PARAMETER(m_k      , FE_PARAM_DOUBLE  , "penalty" );
	ADD_PARAMETER(m_blaugon, FE_PARAM_BOOL    , "laugon"  );
	ADD_PARAMETER(m_altol  , FE_PARAM_DOUBLE  , "altol"   );
	ADD_PARAMETERV(m_r0    , FE_PARAM_DOUBLEV, 3, "range_min");
	ADD_PARAMETERV(m_r1    , FE_PARAM_DOUBLEV, 3, "range_max");
        // this enables time shifting of phase evaluation
	ADD_PARAMETER(m_update_t  , FE_PARAM_DOUBLE , "template_time" );
END_PARAMETER_LIST();

//-----------------------------------------------------------------------------
FEWarpImageConstraint::FEWarpImageConstraint(FEModel* pfem) : FEWarpConstraint(pfem), m_tp1map(m_tp1), m_tp2map(m_tp2), m_tp3map(m_tp3), m_sp1map(m_sp1), m_sp2map(m_sp2), m_sp3map(m_sp3), m_tm1map(m_tm1), m_tm2map(m_tm2), m_tm3map(m_tm3), m_sm1map(m_sm1), m_sm2map(m_sm2), m_sm3map(m_sm3)
{
	
}

//-----------------------------------------------------------------------------
bool FEWarpImageConstraint::Init()
{
	FEWarpConstraint::Init();

	int nx = m_tp1.width ();
	int ny = m_tp1.height();
	int nz = m_tp1.depth ();

	vec3d r0(m_r0[0], m_r0[1], m_r0[2]);
	vec3d r1(m_r1[0], m_r1[1], m_r1[2]);

	// setting range across all images
	m_tp1map.SetRange(r0, r1);
	m_tp2map.SetRange(r0, r1);
	m_tp3map.SetRange(r0, r1);
	m_sp1map.SetRange(r0, r1);
	m_sp2map.SetRange(r0, r1);
	m_sp3map.SetRange(r0, r1);

	m_tm1map.SetRange(r0, r1);
	m_tm2map.SetRange(r0, r1);
	m_tm3map.SetRange(r0, r1);
	m_sm1map.SetRange(r0, r1);
	m_sm2map.SetRange(r0, r1);
	m_sm3map.SetRange(r0, r1);

	return true;
}

//-----------------------------------------------------------------------------
// This is called at the beginning of each time step
// NOTE: This is currently called at the end of each iteration as well.
//       In future versions of FEBio this will no longer be the case.
void FEWarpImageConstraint::Update()
{

}

//-----------------------------------------------------------------------------
vec3d FEWarpImageConstraint::wrpForce(FEMaterialPoint& mp)
{
	FEElasticMaterialPoint& pt = *mp.ExtractData<FEElasticMaterialPoint>();
		
	// evaluate template phase images
	double T_p[3];
	T_p[0] = m_tp1map.value(pt.m_r0);
	T_p[1] = m_tp2map.value(pt.m_r0);
	T_p[2] = m_tp3map.value(pt.m_r0);

	// evaluate template phase images
	double S_p[3];
	S_p[0] = m_sp1map.value(pt.m_rt);
	S_p[1] = m_sp2map.value(pt.m_rt);
	S_p[2] = m_sp3map.value(pt.m_rt);

	// evaluate image flags
	bool Out_p[2];
	Out_p[0] = m_tp1map.outflag(pt.m_r0);
	Out_p[1] = m_sp1map.outflag(pt.m_rt);

	// define force only if inside imaging domain
	vec3d Fw;

	if (Out_p[0]||Out_p[1]){
		Fw.x = 0;
 		Fw.y = 0;
 		Fw.z = 0;
	}
	else {
		Fw.x = wrap(S_p[0] - T_p[0]);
 		Fw.y = wrap(S_p[1] - T_p[1]);
 		Fw.z = wrap(S_p[2] - T_p[2]);
	}

	return Fw*m_k;
	
}

//-----------------------------------------------------------------------------
mat3d FEWarpImageConstraint::wrpStiffness(FEMaterialPoint& mp)
{
	FEElasticMaterialPoint& pt = *mp.ExtractData<FEElasticMaterialPoint>();

	// Calculate target gradients
	vec3d dS1 = -m_sp1map.gradient(pt.m_rt);
	vec3d dS2 = -m_sp2map.gradient(pt.m_rt);
	vec3d dS3 = -m_sp3map.gradient(pt.m_rt);  

	// evaluate image flags
	bool Out_p[2];
	Out_p[0] = m_tp1map.outflag(pt.m_r0);
	Out_p[1] = m_sp1map.outflag(pt.m_rt);

	// warping stiffness defined only if inside the imaging domain
	mat3d K;

	K.zero();

	if (Out_p[0]||Out_p[1]){

	} 
	else {
            K(0,0) = dS1.x; K(0,1) = dS1.y; K(0,2) = dS1.z;
            K(1,0) = dS2.x; K(1,1) = dS2.y; K(1,2) = dS2.z;
            K(2,0) = dS3.x; K(2,1) = dS3.y; K(2,2) = dS3.z;
	}

	return K*m_k;

}
// local phase unwrapping
double FEWarpImageConstraint::wrap(double phase){
	double w = fmod(phase + PI, 2*PI);

	if (w<0){
		w += 2*PI;
	}

	return w - PI;
}