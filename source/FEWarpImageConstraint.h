#pragma once
#include "FEWarpConstraint.h"

#include "stdafx.h" // debug
#include <iostream> // debug
using namespace std; //debug

//-----------------------------------------------------------------------------
class FEWarpImageConstraint : public FEWarpConstraint
{
public:
	FEWarpImageConstraint(FEModel* pfem);
	~FEWarpImageConstraint() {}

	// initialization
	bool Init();

	// update
	void Update();

public:
	// phase maps
	PhaseMap& GetTP1Map() { return m_tp1map; }
	PhaseMap& GetTP2Map() { return m_tp2map; }
	PhaseMap& GetTP3Map() { return m_tp3map; }
	PhaseMap& GetSP1Map() { return m_sp1map; }
	PhaseMap& GetSP2Map() { return m_sp2map; }
	PhaseMap& GetSP3Map() { return m_sp3map; }
        // image maps
	ImageMap& GetTM1Map() { return m_tm1map; }
	ImageMap& GetTM2Map() { return m_tm2map; }
	ImageMap& GetTM3Map() { return m_tm3map; }
	ImageMap& GetSM1Map() { return m_sm1map; }
	ImageMap& GetSM2Map() { return m_sm2map; }
	ImageMap& GetSM3Map() { return m_sm3map; }

	void ShallowCopy(DumpStream& dmp, bool bsave) {};

public:
	double wrap(double phase);

	//! Calculate the force at a material point
	vec3d wrpForce(FEMaterialPoint& pt);

	//! calculate the stiffness at a material point
	mat3d wrpStiffness(FEMaterialPoint& pt);

protected:
	
	double	m_r0[3];	//!< minimum range
	double	m_r1[3];	//!< maximum range
	
	// adding phase images
	Image m_tp1;
	Image m_tp2;
	Image m_tp3;
	Image m_sp1;	
	Image m_sp2;
	Image m_sp3;

	Image m_tm1;	
	Image m_tm2;
	Image m_tm3;
	Image m_sm1;
	Image m_sm2;
	Image m_sm3;

	// adding phase maps
	PhaseMap m_tp1map; 
	PhaseMap m_tp2map; 
	PhaseMap m_tp3map; 	
	PhaseMap m_sp1map; 
	PhaseMap m_sp2map; 
	PhaseMap m_sp3map; 

	ImageMap m_tm1map;
	ImageMap m_tm2map;
	ImageMap m_tm3map;
	ImageMap m_sm1map;
	ImageMap m_sm2map;
	ImageMap m_sm3map;

public:
	DECLARE_PARAMETER_LIST();
};
