#include "stdafx.h"
#include "FECore/FESolidDomain.h"
#include "FEWarpPlot.h"
#include "ImageMap.h"
#include "PhaseMap.h"
#include "FEWarpConstraint.h"
#include "FEWarpImageConstraint.h"
#include <FECore/FEModel.h>
#include <FEBioMech/FEElasticMaterial.h>


// template phase images
bool FEPlotPhaseTemplate::Save(FEMesh &m, std::vector<float> &a)
{
	// find the warping constraint
	FEModel& fem = *m_pfem;
	FEWarpImageConstraint* pc = 0;
	for (int i=0; i<fem.NonlinearConstraints(); ++i)
	{
		pc = dynamic_cast<FEWarpImageConstraint*>(fem.NonlinearConstraint(i));
		if (pc) break;
	}
	if (pc == 0) return false;

	// phase template maps
	PhaseMap& m_tp1map = pc->GetTP1Map();
	PhaseMap& m_tp2map = pc->GetTP2Map();
	PhaseMap& m_tp3map = pc->GetTP3Map();

	// delayed update function
	std::vector<vec3d>& m_N_Data = pc->GetN_data();

	int N = m.Nodes();
	for (int i=0; i<N; ++i) 
	{
		// delayed template
		vec3d r0 = m_N_Data[i];
		vec3d rt = m.Node(i).m_rt;
		
		// evaluate template phase images
		vec3d T_p;
		T_p.x = m_tp1map.value(r0);
 		T_p.y = m_tp2map.value(r0);
 		T_p.z = m_tp3map.value(r0);

		// store
		a.push_back((float) T_p.x);
		a.push_back((float) T_p.y);
		a.push_back((float) T_p.z);
	}
	return true;
}

// template tagged images
bool FEPlotTagTemplate::Save(FEMesh &m, std::vector<float> &a)
{
	// find the warping constraint
	FEModel& fem = *m_pfem;
	FEWarpImageConstraint* pc = 0;
	for (int i=0; i<fem.NonlinearConstraints(); ++i)
	{
		pc = dynamic_cast<FEWarpImageConstraint*>(fem.NonlinearConstraint(i));
		if (pc) break;
	}
	if (pc == 0) return false;

	// image template maps
	ImageMap& m_tm1map = pc->GetTM1Map();
	ImageMap& m_tm2map = pc->GetTM2Map();
	ImageMap& m_tm3map = pc->GetTM3Map();

	// delayed update function
	std::vector<vec3d>& m_N_Data = pc->GetN_data();

	// loop through nodes
	int N = m.Nodes();
	for (int i=0; i<N; ++i) 
	{
		// delayed update function
		vec3d r0 = m_N_Data[i];

		// evaluate template phase images
		vec3d T_m;
		T_m.x = m_tm1map.value(r0);
 		T_m.y = m_tm2map.value(r0);
 		T_m.z = m_tm3map.value(r0);

		// store
		a.push_back((float) T_m.x);
		a.push_back((float) T_m.y);
		a.push_back((float) T_m.z);
	} 
	return true;
}

// target phase images
bool FEPlotPhaseTarget::Save(FEMesh &m, std::vector<float> &a)
{
	// find the warping constraint
	FEModel& fem = *m_pfem;
	FEWarpImageConstraint* pc = 0;
	for (int i=0; i<fem.NonlinearConstraints(); ++i)
	{
		pc = dynamic_cast<FEWarpImageConstraint*>(fem.NonlinearConstraint(i));
		if (pc) break;
	}
	if (pc == 0) return false;

	// phase target maps
	PhaseMap& m_sp1map = pc->GetSP1Map();
	PhaseMap& m_sp2map = pc->GetSP2Map();
	PhaseMap& m_sp3map = pc->GetSP3Map();

	int N = m.Nodes();
	for (int i=0; i<N; ++i) 
	{
		vec3d r0 = m.Node(i).m_r0;
		vec3d rt = m.Node(i).m_rt;
		
		// evaluate target phase images
		vec3d S_p;
		S_p.x = m_sp1map.value(rt);
 		S_p.y = m_sp2map.value(rt);
 		S_p.z = m_sp3map.value(rt);

		// store
		a.push_back((float) S_p.x);
		a.push_back((float) S_p.y);
		a.push_back((float) S_p.z);
	}
	return true;
}


// target tagged images
bool FEPlotTagTarget::Save(FEMesh &m, std::vector<float> &a)
{
	// find the warping constraint
	FEModel& fem = *m_pfem;
	FEWarpImageConstraint* pc = 0;
	for (int i=0; i<fem.NonlinearConstraints(); ++i)
	{
		pc = dynamic_cast<FEWarpImageConstraint*>(fem.NonlinearConstraint(i));
		if (pc) break;
	}
	if (pc == 0) return false;

	// image target maps
	ImageMap& m_sm1map = pc->GetSM1Map();
	ImageMap& m_sm2map = pc->GetSM2Map();
	ImageMap& m_sm3map = pc->GetSM3Map();

	int N = m.Nodes();
	for (int i=0; i<N; ++i) 
	{
		
		vec3d r0 = m.Node(i).m_r0;
		vec3d rt = m.Node(i).m_rt;
		
		// evaluate template phase images
		vec3d S_m;
		S_m.x = m_sm1map.value(rt);
 		S_m.y = m_sm2map.value(rt);
 		S_m.z = m_sm3map.value(rt);

		// store
		a.push_back((float) S_m.x);
		a.push_back((float) S_m.y);
		a.push_back((float) S_m.z);
	}
	return true;
}

bool FEPlotForce::Save(FEMesh &m, std::vector<float> &a)
{
	// find the warping constraint
	FEModel& fem = *m_pfem;
	FEWarpImageConstraint* pc = 0;
	for (int i=0; i<fem.NonlinearConstraints(); ++i)
	{
		pc = dynamic_cast<FEWarpImageConstraint*>(fem.NonlinearConstraint(i));
		if (pc) break;
	}
	if (pc == 0) return false;

	// phase maps
	PhaseMap& m_tp1map = pc->GetTP1Map();
	PhaseMap& m_tp2map = pc->GetTP2Map();
	PhaseMap& m_tp3map = pc->GetTP3Map();
	PhaseMap& m_sp1map = pc->GetSP1Map();
	PhaseMap& m_sp2map = pc->GetSP2Map();
	PhaseMap& m_sp3map = pc->GetSP3Map();

	// delayed update function
	std::vector<vec3d>& m_N_Data = pc->GetN_data();

	int N = m.Nodes();
	for (int i=0; i<N; ++i) 
	{	
		vec3d r0 = m_N_Data[i];
		// vec3d r0 = m.Node(i).m_r0;
		vec3d rt = m.Node(i).m_rt;
		
		// evaluate template phase images
		double T_p[3];
		T_p[0] = m_tp1map.value(r0);
		T_p[1] = m_tp2map.value(r0);
		T_p[2] = m_tp3map.value(r0);

		// evaluate template phase images
		double S_p[3];
		S_p[0] = m_sp1map.value(rt);
		S_p[1] = m_sp2map.value(rt);
		S_p[2] = m_sp3map.value(rt);

		// define force 
		vec3d fw;
		fw.x = wrap(S_p[0] - T_p[0]);
 		fw.y = wrap(S_p[1] - T_p[1]);
 		fw.z = wrap(S_p[2] - T_p[2]);

		// store
		a.push_back((float) fw.x);
		a.push_back((float) fw.y);
		a.push_back((float) fw.z);
	}
	return true;
}

double FEPlotForce::wrap(double phase){
	double w = fmod(phase + PI, 2*PI);

	if (w<0){
		w += 2*PI;
	}

	return w - PI;
}

// energy
bool FEPlotPhaseResidual::Save(FEMesh &m, std::vector<float> &a)
{
	// find the warping constraint
	FEModel& fem = *m_pfem;
	FEWarpImageConstraint* pc = 0;
	for (int i=0; i<fem.NonlinearConstraints(); ++i)
	{
		pc = dynamic_cast<FEWarpImageConstraint*>(fem.NonlinearConstraint(i));
		if (pc) break;
	}
	if (pc == 0) return false;

	// phase maps
	PhaseMap& m_tp1map = pc->GetTP1Map();
	PhaseMap& m_tp2map = pc->GetTP2Map();
	PhaseMap& m_tp3map = pc->GetTP3Map();
	PhaseMap& m_sp1map = pc->GetSP1Map();
	PhaseMap& m_sp2map = pc->GetSP2Map();
	PhaseMap& m_sp3map = pc->GetSP3Map();

	// delayed update function
	std::vector<vec3d>& m_N_Data = pc->GetN_data();

	int N = m.Nodes();
	for (int i=0; i<N; ++i) 
	{	
		// vec3d r0 = m.Node(i).m_r0;
		vec3d r0 = m_N_Data[i]; 
		vec3d rt = m.Node(i).m_rt;
		
		// evaluate template phase images
		double T_p[3];
		T_p[0] = m_tp1map.value(r0);
		T_p[1] = m_tp2map.value(r0);
		T_p[2] = m_tp3map.value(r0);

		// evaluate template phase images
		double S_p[3];
		S_p[0] = m_sp1map.value(rt);
		S_p[1] = m_sp2map.value(rt);
		S_p[2] = m_sp3map.value(rt);

		// define force 
		double E;
		E = abs(wrap(abs(wrap(T_p[0] - S_p[0])) + abs(wrap(T_p[1] - S_p[1])) + abs(wrap(T_p[2] - S_p[2]))));

		// store
		a.push_back((float) E);
	}
	return true;
}
// local phase unwrapping
double FEPlotPhaseResidual::wrap(double phase){
	double w = fmod(phase + PI, 2*PI);

	if (w<0){
		w += 2*PI;
	}

	return w - PI;
}
