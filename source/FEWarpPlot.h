#pragma once

#include "FECore/FEPlotData.h"
#include "FEWarpImageConstraint.h"

// adding phase image (8/14  - go to FEWarpPlot.cpp)

// template phase images
class FEPlotPhaseTemplate : public FENodeData
{
public:
	FEPlotPhaseTemplate(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// template tagged images
class FEPlotTagTemplate : public FENodeData
{
public:
	FEPlotTagTemplate(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
	// vector<int>	m_dom;		// list of domains to warp MGTI
};

// target phase images
class FEPlotPhaseTarget : public FENodeData
{
public:
	FEPlotPhaseTarget(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// target tagged images
class FEPlotTagTarget : public FENodeData
{
public:
	FEPlotTagTarget(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// tag tracking force
class FEPlotForce : public FENodeData
{
public:
	FEPlotForce(FEModel* pfem) : FENodeData(PLT_VEC3F, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};

// energy
class FEPlotPhaseResidual : public FENodeData
{
public:
	FEPlotPhaseResidual(FEModel* pfem) : FENodeData(PLT_FLOAT, FMT_NODE), m_pfem(pfem) {}
	virtual bool Save(FEMesh& m, vector<float>& a);

protected:
	FEModel*	m_pfem;
	double wrap(double phase);
};