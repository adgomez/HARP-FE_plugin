#include "stdafx.h"
#include "PhaseMap.h"

PhaseMap::PhaseMap(Image& img) : m_img(img)
{
	m_r0 = vec3d(0,0,0);
	m_r1 = vec3d(0,0,0);
}

PhaseMap::~PhaseMap(void)
{
}

void PhaseMap::SetRange(vec3d r0, vec3d r1)
{
	m_r0 = r0;
	m_r1 = r1;
}

PhaseMap::POINT PhaseMap::map(vec3d p)
{
	int nx = m_img.width ();
	int ny = m_img.height();
	int nz = m_img.depth ();

	double x = (p.x - m_r0.x)/(m_r1.x - m_r0.x);
	double y = (p.y - m_r0.y)/(m_r1.y - m_r0.y);
	double z = (nz==1?0:(p.z - m_r0.z)/(m_r1.z - m_r0.z));

	double cx = 1.0 / (double) (nx - 1);
	double cy = 1.0 / (double) (ny - 1);
	double cz = (nz>1?1.0 / (double) (nz - 1):1.0);

	int i = (int) floor(x*(nx - 1));
	int j = (int) floor(y*(ny - 1));
	int k = (int) floor(z*(nz - 1));

	if (i == nx-1) i--;
	if (j == ny-1) j--;
	if ((k == nz-1)&&(nz>1)) k--;

	double r = (x - i*cx)/cx;
	double s = (y - j*cy)/cy;
	double t = (z - k*cz)/cz;

	POINT pt;

	// nearest neighbor
	pt.i = i;
	pt.j = j;
	pt.k = k;

	// fractional residuals
	pt.r = r;
	pt.s = s;
	pt.t = t;

	// shape function weights (pixel cube is treated like a linear hex)
	if (nz == 1)
	{
		pt.h[0] = (1-r)*(1-s);
		pt.h[1] = r*(1-s);
		pt.h[2] = r*s;
		pt.h[3] = s*(1-r);
		pt.h[4] = pt.h[5] = pt.h[6] = pt.h[7] = 0.0;
	}
	else
	{
		pt.h[0] = (1-r)*(1-s)*(1-t);
		pt.h[1] = r*(1-s)*(1-t);
		pt.h[2] = r*s*(1-t);
		pt.h[3] = s*(1-r)*(1-t);
		pt.h[4] = (1-r)*(1-s)*t;
		pt.h[5] = r*(1-s)*t;
		pt.h[6] = r*s*t;
		pt.h[7] = s*(1-r)*t;
	}

	return pt;
}

bool PhaseMap::outflag(const vec3d& p){

	bool flag = true;

	if (m_img.depth()>1.0) {
		if ((m_r0.x <= p.x)&&(p.x <= m_r1.x)&&
			(m_r0.y <= p.y)&&(p.y <= m_r1.y)&&
			(m_r0.z <= p.z)&&(p.z <= m_r1.z)) {
			flag = false;
		}
	}
	else{
		if ((m_r0.x <= p.x)&&(p.x <= m_r1.x)&&
			(m_r0.y <= p.y)&&(p.y <= m_r1.y)) {
			flag = false;
		}
	}

	return flag;
}

double PhaseMap::value(const POINT& p)
{
	int nx = m_img.width ();
	int ny = m_img.height();
	int nz = m_img.depth ();

	// NOTE: phase values are normalized to [-pi pi)
	if (nz == 1)
	{
		// out of image exeption
		if ((p.i<0) || (p.i >= nx-1)) return 0.0;
		if ((p.j<0) || (p.j >= ny-1)) return 0.0;
		
		// wrapped bilinear interpolation
		double v[4];
		v[0] = (m_img.value(p.i  , p.j  , 0)*2 - 1)*PI;
		v[1] = (m_img.value(p.i+1, p.j  , 0)*2 - 1)*PI;
		v[2] = (m_img.value(p.i+1, p.j+1, 0)*2 - 1)*PI;
		v[3] = (m_img.value(p.i  , p.j+1, 0)*2 - 1)*PI;

		double dv[3];
		dv[0] = wrap(v[1] - v[0]); 
		dv[1] = wrap(v[3] - v[0]); 
		dv[2] = wrap(wrap(v[2] - v[3]) - dv[0]); 

		return wrap(v[0] + p.r*dv[0] + p.s*dv[1]  + p.r*p.s*dv[2]); 
		
	}
	else
	{
		// out of image exeption
		if ((p.i<0) || (p.i >= nx-1)) return 0.0;
		if ((p.j<0) || (p.j >= ny-1)) return 0.0;
		if ((p.k<0) || (p.k >= nz-1)) return 0.0;

		// wrapped trilinear interpolation
		double v[8];
		v[0] = (m_img.value(p.i  , p.j  , p.k  )*2 - 1)*PI;
		v[1] = (m_img.value(p.i+1, p.j  , p.k  )*2 - 1)*PI;
		v[2] = (m_img.value(p.i+1, p.j+1, p.k  )*2 - 1)*PI;
		v[3] = (m_img.value(p.i  , p.j+1, p.k  )*2 - 1)*PI;
		v[4] = (m_img.value(p.i  , p.j  , p.k+1)*2 - 1)*PI;
		v[5] = (m_img.value(p.i+1, p.j  , p.k+1)*2 - 1)*PI;
		v[6] = (m_img.value(p.i+1, p.j+1, p.k+1)*2 - 1)*PI;
		v[7] = (m_img.value(p.i  , p.j+1, p.k+1)*2 - 1)*PI;

		double dv[7];
		dv[0] = wrap(v[1] - v[0]); 
		dv[1] = wrap(v[3] - v[0]);
		dv[2] = wrap(v[4] - v[0]);
		dv[3] = wrap(wrap(v[2] - v[3]) - dv[0]);
		dv[4] = wrap(wrap(v[5] - v[1]) - dv[2]);
		dv[5] = wrap(wrap(v[7] - v[4]) - dv[1]);
		dv[6] = wrap(wrap(v[6] + v[1] + v[3] + v[4]) - wrap(v[0] + v[2] + v[5] + v[7])); 

		return wrap(v[0] + 
					p.r*dv[0] + 
					p.s*dv[1] + 
					p.t*dv[2] + 
					p.r*p.s*dv[3] + 
					p.r*p.t*dv[4] + 
					p.s*p.t*dv[5] + 
					p.r*p.s*p.t*dv[6]);
	}
}

vec3d PhaseMap::gradient(const vec3d& r)
{
	POINT p = map(r);

	if (m_img.depth() == 1)
	{
		// get the x-gradient values
		double gx[4];
		gx[0] = grad_x(p.i  , p.j  , 0);
		gx[1] = grad_x(p.i+1, p.j  , 0);
		gx[2] = grad_x(p.i+1, p.j+1, 0);
		gx[3] = grad_x(p.i  , p.j+1, 0);

		// get the y-gradient values
		double gy[4];
		gy[0] = grad_y(p.i  , p.j  , 0);
		gy[1] = grad_y(p.i+1, p.j  , 0);
		gy[2] = grad_y(p.i+1, p.j+1, 0);
		gy[3] = grad_y(p.i  , p.j+1, 0);

		// get the localy unwrapped x-gradient values
		double wx[4];
		wx[0] = wgrad_x(p.i  , p.j  , 0);
		wx[1] = wgrad_x(p.i+1, p.j  , 0);
		wx[2] = wgrad_x(p.i+1, p.j+1, 0);
		wx[3] = wgrad_x(p.i  , p.j+1, 0);

		// get the localy unwrapped y-gradient values
		double wy[4];
		wy[0] = wgrad_y(p.i  , p.j  , 0);
		wy[1] = wgrad_y(p.i+1, p.j  , 0);
		wy[2] = wgrad_y(p.i+1, p.j+1, 0);
		wy[3] = wgrad_y(p.i  , p.j+1, 0);

		// get the gradient at point r
		// (for now, let's use nearest neighbor)
		double hx = gx[0]; if (abs(gx[0])>abs(wx[0])) { hx = wx[0]; }
		double hy = gy[0]; if (abs(gy[0])>abs(wy[0])) { hy = wy[0]; }

		double Dx = dx();
		double Dy = dy();

		return vec3d(hx/Dx, hy/Dy, 0.0);
	}
	else
	{
		// get the x-gradient values
		double gx[8];
		gx[0] = grad_x(p.i  , p.j  , p.k  );
		gx[1] = grad_x(p.i+1, p.j  , p.k  );
		gx[2] = grad_x(p.i+1, p.j+1, p.k  );
		gx[3] = grad_x(p.i  , p.j+1, p.k  );
		gx[4] = grad_x(p.i  , p.j  , p.k+1);
		gx[5] = grad_x(p.i+1, p.j  , p.k+1);
		gx[6] = grad_x(p.i+1, p.j+1, p.k+1);
		gx[7] = grad_x(p.i  , p.j+1, p.k+1);

		// get the y-gradient values
		double gy[8];
		gy[0] = grad_y(p.i  , p.j  , p.k  );
		gy[1] = grad_y(p.i+1, p.j  , p.k  );
		gy[2] = grad_y(p.i+1, p.j+1, p.k  );
		gy[3] = grad_y(p.i  , p.j+1, p.k  );
		gy[4] = grad_y(p.i  , p.j  , p.k+1);
		gy[5] = grad_y(p.i+1, p.j  , p.k+1);
		gy[6] = grad_y(p.i+1, p.j+1, p.k+1);
		gy[7] = grad_y(p.i  , p.j+1, p.k+1);

		// get the z-gradient values
		double gz[8];
		gz[0] = grad_z(p.i  , p.j  , p.k  );
		gz[1] = grad_z(p.i+1, p.j  , p.k  );
		gz[2] = grad_z(p.i+1, p.j+1, p.k  );
		gz[3] = grad_z(p.i  , p.j+1, p.k  );
		gz[4] = grad_z(p.i  , p.j  , p.k+1);
		gz[5] = grad_z(p.i+1, p.j  , p.k+1);
		gz[6] = grad_z(p.i+1, p.j+1, p.k+1);
		gz[7] = grad_z(p.i  , p.j+1, p.k+1);

		// get the localy unwrapped x-gradient values
		double wx[8];
		wx[0] = wgrad_x(p.i  , p.j  , p.k  );
		wx[1] = wgrad_x(p.i+1, p.j  , p.k  );
		wx[2] = wgrad_x(p.i+1, p.j+1, p.k  );
		wx[3] = wgrad_x(p.i  , p.j+1, p.k  );
		wx[4] = wgrad_x(p.i  , p.j  , p.k+1);
		wx[5] = wgrad_x(p.i+1, p.j  , p.k+1);
		wx[6] = wgrad_x(p.i+1, p.j+1, p.k+1);
		wx[7] = wgrad_x(p.i  , p.j+1, p.k+1);

		// get the localy unwrapped y-gradient values
		double wy[8];
		wy[0] = wgrad_y(p.i  , p.j  , p.k  );
		wy[1] = wgrad_y(p.i+1, p.j  , p.k  );
		wy[2] = wgrad_y(p.i+1, p.j+1, p.k  );
		wy[3] = wgrad_y(p.i  , p.j+1, p.k  );
		wy[4] = wgrad_y(p.i  , p.j  , p.k+1);
		wy[5] = wgrad_y(p.i+1, p.j  , p.k+1);
		wy[6] = wgrad_y(p.i+1, p.j+1, p.k+1);
		wy[7] = wgrad_y(p.i  , p.j+1, p.k+1);

		// get the localy unwrapped z-gradient values
		double wz[8];
		wz[0] = wgrad_z(p.i  , p.j  , p.k  );
		wz[1] = wgrad_z(p.i+1, p.j  , p.k  );
		wz[2] = wgrad_z(p.i+1, p.j+1, p.k  );
		wz[3] = wgrad_z(p.i  , p.j+1, p.k  );
		wz[4] = wgrad_z(p.i  , p.j  , p.k+1);
		wz[5] = wgrad_z(p.i+1, p.j  , p.k+1);
		wz[6] = wgrad_z(p.i+1, p.j+1, p.k+1);
		wz[7] = wgrad_z(p.i  , p.j+1, p.k+1);


		// get the gradient at point r
		// (for now, let's use nearest neighbor)
		double hx = gx[0]; if (abs(gx[0])>abs(wx[0])) { hx = wx[0]; }
		double hy = gy[0]; if (abs(gy[0])>abs(wy[0])) { hy = wy[0]; }
		double hz = gz[0]; if (abs(gz[0])>abs(wz[0])) { hz = wz[0]; }

		double Dx = dx();
		double Dy = dy();
		double Dz = dz();

		return vec3d(hx/Dx, hy/Dy, hz/Dz);
	}
}

// this is a support method to help with debugging
double PhaseMap::OutCast(const vec3d& r){

	return value(map(r));
}

double PhaseMap::wrap(double phase){
	double w = fmod(phase + PI, 2*PI);

	if (w<0){
		w += 2*PI;
	}

	return w - PI;
}

double PhaseMap::grad_x(int i, int j, int k)
{
	int nx = m_img.width();

	double v[3];
	v[0] = (m_img.value(i-1, j  , k  )*2 - 1)*PI;
	v[1] = (m_img.value(i  , j  , k  )*2 - 1)*PI;
	v[2] = (m_img.value(i+1, j  , k  )*2 - 1)*PI;

	if (i == 0   ) return v[2] - v[1];
	if (i == nx-1) return v[1] - v[0];

	return 0.5*(v[2] - v[0]);
}

double PhaseMap::grad_y(int i, int j, int k)
{
	int ny = m_img.height();

	double v[3];
	v[0] = (m_img.value(i  , j-1, k  )*2 - 1)*PI;
	v[1] = (m_img.value(i  , j  , k  )*2 - 1)*PI;
	v[2] = (m_img.value(i  , j+1, k  )*2 - 1)*PI;

	if (j == 0   ) return v[2] - v[1];
	if (j == ny-1) return v[1] - v[0];

	return 0.5*(v[2] - v[0]);
}

double PhaseMap::grad_z(int i, int j, int k)
{
	int nz = m_img.depth();
	if (nz == 1) return 0.0;

	double v[3];
	v[0] = (m_img.value(i  , j  , k-1)*2 - 1)*PI;
	v[1] = (m_img.value(i  , j  , k  )*2 - 1)*PI;
	v[2] = (m_img.value(i  , j  , k+1)*2 - 1)*PI;

	if (k == 0   ) return v[2] - v[1];
	if (k == nz-1) return v[1] - v[0];

	return 0.5*(v[2] - v[0]);
}

double PhaseMap::wgrad_x(int i, int j, int k)
{
	int nx = m_img.width();

	double v[3];
	v[0] = wrap((m_img.value(i-1, j  , k  )*2 - 1)*PI + PI);
	v[1] = wrap((m_img.value(i  , j  , k  )*2 - 1)*PI + PI);
	v[2] = wrap((m_img.value(i+1, j  , k  )*2 - 1)*PI + PI);

	if (i == 0   ) return wrap(v[2] - v[1]);
	if (i == nx-1) return wrap(v[1] - v[0]);

	return 0.5*(v[2] - v[0]);
}

double PhaseMap::wgrad_y(int i, int j, int k)
{
	int ny = m_img.height();

	double v[3];
	v[0] = wrap((m_img.value(i  , j-1, k  )*2 - 1)*PI + PI);
	v[1] = wrap((m_img.value(i  , j  , k  )*2 - 1)*PI + PI);
	v[2] = wrap((m_img.value(i  , j+1, k  )*2 - 1)*PI + PI);

	if (j == 0   ) return wrap(v[2] - v[1]);
	if (j == ny-1) return wrap(v[1] - v[0]);

	return 0.5*(v[2] - v[0]);
}

double PhaseMap::wgrad_z(int i, int j, int k)
{
	int nz = m_img.depth();
	if (nz == 1) return 0.0;

	double v[3];
	v[0] = wrap((m_img.value(i  , j  , k-1)*2 - 1)*PI + PI);
	v[1] = wrap((m_img.value(i  , j  , k  )*2 - 1)*PI + PI);
	v[2] = wrap((m_img.value(i  , j  , k+1)*2 - 1)*PI + PI);

	if (k == 0   ) return wrap(v[2] - v[1]);
	if (k == nz-1) return wrap(v[1] - v[0]);

	return 0.5*(v[2] - v[0]);
}
